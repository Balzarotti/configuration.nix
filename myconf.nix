{ config, pkgs, lib, ... }:
let
  mpc_password = "remoteMPD1299";

  custom_mpc = pkgs.callPackage ./nx/bin/mpc.nix
  { password = mpc_password; };
  audio_save = pkgs.callPackage ./nx/bin/audio-save.nix { };

  custom_i3lock = pkgs.callPackage ./nx/bin/i3lock.nix { mpc = custom_mpc; };

  custom_ncmpcpp = pkgs.callPackage ./nx/bin/ncmpcpp.nix
  { password = mpc_password; };

  brightness_control = pkgs.callPackage ./nx/bin/brighness-control.nix {
                        xbacklight= pkgs.xorg.xbacklight;
			};

  i3_config = pkgs.callPackage ./nx/configs/i3tmp.nix {
    mpc_command = "${custom_mpc}/bin/custom-mpc";
    brightness_control = "${brightness_control}/bin/brightness-control";
    i3lock_pkg = "${custom_i3lock}/bin/custom-i3lock";
    modifier = "Mod4";
    terminal = "${pkgs.rxvt_unicode}/bin/urxvt";
    terminalCommandParam = "-e";
    fileManager = "${pkgs.pcmanfm}/bin/pcmanfm";
    saveWinName = "${pkgs.xdotool}/bin/xdotool getactivewindow getwindowname > /tmp/.last-win-name";
    saveWinCmd = "cat /proc/$(${pkgs.xdotool}/bin/xdotool getactivewindow getwindowpid)/cmdline | tr '\\000' ' ' > /tmp/.last-win-cmd";
    touchpad_disable_timeout = "0.3";
    mailTimeout = "30";
  };

  zsh_config = pkgs.callPackage ./nx/configs/zsh.nix {
    now = "${pkgs.coreutils}/bin/date +%H.%M:%S";
  };

  tmux_config = pkgs.callPackage ./nx/configs/tmux.nix {};

  mpdscribble_config = pkgs.callPackage ./nx/configs/mpdscribble.nix {
    username = "nico202";
    mpc_password = mpc_password;
  };

  mbsync_config = pkgs.callPackage ./nx/configs/mbsyncrc.nix {};
  numix_solarized-gtk-theme = with pkgs; stdenv.mkDerivation rec {
    version = "0.0.1";
    name = "numix_solarized-gtk-theme-${version}";
    src = fetchFromGitHub {
      owner = "Ferdi265";
      repo = "numix-solarized-gtk-theme";
      rev = "b6fce07c95a84dd618ed8594d441d0543192c1bb";
      sha256 = "0gix5gx0n2bqyig3ngc9ky4296kqhnjr2vzjkidsjlkqp8raalmw";
    };
# should be build dependency, maybe, but as buildDependency it doesn't see sass executable in PATH
    buildInputs = [ gdk_pixbuf glib sass libxml2 ];
    buildPhase = ''
        make DESTDIR=$out
    '';
    prePatch = ''
        sed -i 's/Numix/Numix Solarized/g' index.theme
        export bkgrn=586e75
        export white=eee8d5
        export black=002b36
        sed -i 's/selected_bg_color: #[^\\n]*;/selected_bg_color: #'$bkgrn';/g' gtk-3.0/scss/_global.scss
        sed -i 's/selected_bg_color: #[^\\n]*;/selected_bg_color: #'$bkgrn';/g' gtk-3.20/scss/_global.scss
        sed -i 's/white: #fff;/white: #'$white';/g' gtk-3.0/scss/_global.scss
        sed -i 's/white: #fff;/white: #'$white';/g' gtk-3.20/scss/_global.scss
        sed -i 's/black: #000;/black: #'$black';/g' gtk-3.0/scss/_global.scss
        sed -i 's/black: #000;/black: #'$black';/g' gtk-3.20/scss/_global.scss
        sed -i 's/nselected_bg_color:#[^\\n]*\\n/nselected_bg_color:#'$bkgrn'\\n/g' gtk-2.0/gtkrc
    '';
    installPhase = ''
      install -dm 755 $out/share/themes/Numix\ Solarized
      cp -dr --no-preserve='ownership' {LICENSE,CREDITS,index.theme,README.md,gtk-2.0,gtk-3.0,gtk-3.20,metacity-1,openbox-3,unity,xfce-notify-4.0,xfwm4} $out/share/themes/Numix\ Solarized/
    '';
    meta = {
      description = "Numix Solarized GTK theme";
      homepage = http://bitterologist.deviantart.com/art/Numix-Solarized-417575928;
      license = stdenv.lib.licenses.gpl3;
      platforms = stdenv.lib.platforms.all;
    };
  };
in
{
  nx = {
    wm = { # ./wm
          enable = true;
          modifier = "Mod4";
          # configFile = import ./nx/configs/i3tmp.nix;
    };
    # mpd.password = mpc_password;
    realtime = { # ./rtconf
      kernel.enable = false;
      packages.enable = true;
    };
  };

  networking = {
    enableIPv6 = false;
    hostName = "lenovo"; # Define your hostname.
    extraHosts = ''
      127.0.0.1 ${config.networking.hostName}
      146.185.144.154	lipa.ms.mff.cuni.cz
    '';
    wireless.enable = false;  # Enables wireless support via wpa_supplicant.
    firewall.enable = false;

    # Use one of the following
    networkmanager.enable = true;
    wicd.enable = ! config.networking.networkmanager.enable;
    # useDHCP = false;
  };

  hardware = {
    pulseaudio = {
      enable = true;
      # extraClientConf = ''
      #   load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1,localhost
      # '';
      package = pkgs.pulseaudioFull;
    };
    bluetooth.enable = true;

  };

  nix.useSandbox = false;

  programs = {
    zsh = {
      enable = true;
      # Not in current release
      enableAutosuggestions = true;
    };
    adb.enable = true; # add to adbusers
  };

  # FIXME: Go back to "stable" and add master packages with this
  # thanks to: https://blog.jeaye.com/2017/07/30/nixos-revisited/
  # environment.systemPackages = let pkgsUnstable = import
  # (
  #   fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz
  # )
  # { };
  # in
  # [
  #   pkgsUnstable.weechat
  # ];

  environment.systemPackages = with pkgs; [
    # System
    nix-serve # share my package repo

    # mine
    audio_save

    # System enanchements
    tmux tmuxp tmate
    exa # ls replacement
    mosh # mobile sh, replace ssh when low connectivity
    ripgrep # grep
    # Developement
    entr # watch for file changes

    # hardware
    pciutils # find pci sound card
    usbutils # find usb sound card

    # Useful Networking Stuff
    firefox
    torbrowser
    chromium

    nextcloud-client

        # Networking and extra
    wget
    links
    git tig
    ripgrep


    ntfs3g # NTFS write support
    gksu
    file
    rxvt_unicode
    guake
    # gconf.schemas
    xdotool # xmodmap
    xorg.xev # show key press
    xorg.xbacklight
    # hhpc # disabled since sometimes hides the mouse forever

    pavucontrol
    imagemagick

    # Languages
    # R # broken
    python python27Packages.bpython
    # julia-git
    octave

    ghc
    clisp picolisp sbcl

    # System
    htop
    xorg.xkill
    manpages
    nox

    # Pentesting-Cracking
    nmap_graphical
    ettercap
    wireshark-gtk

    p7zip
    artha
    wordnet
    keybase
    kbfs
    sudo

    gnome3.zenity
    gnome3.file-roller
    evince
    recoll

    bashmount
    libnotify # nofity-send
    gnome3.file-roller
    scrot
    xclip
    clipit
    networkmanagerapplet
    # pdftk

    vim # useful and provides xxd

    openjdk
    libreoffice pandoc
    vlc
    # mixxx # broken on unstable?

    blender
    mediainfo

    # Games
    # xonotic

    # proprietary!!
    # (pkgs.callPackage ./pkgs/openhaptics.nix {})
    # (pkgs.callPackage ./pkgs/geomagic_touch.nix {})

    youtube-dl
    isync # provides mbsync
    ( pkgs.thc-hydra.override { withGUI = true; } )
    ( pkgs.handbrake.override { useGtk = true; } )

    ( pkgs.polybar.override {
      mpdSupport = true; mpd_clientlib = pkgs.mpd_clientlib;
      i3Support = true; i3 = pkgs.i3; jsoncpp = pkgs.jsoncpp;
    } )

    ( pkgs.beets.override {enableMpd = true;} )
    mpc_cli mpd ncmpcpp

    numix_solarized-gtk-theme
    # my scripts
    (pkgs.callPackage ./nx/bin/update-mails.nix {})
    (pkgs.callPackage ./nx/bin/check-battery-low.nix {})
    custom_mpc
    custom_ncmpcpp
  ];

  security.pki.certificates = [
"-----BEGIN CERTIFICATE-----
MIIGOTCCBCGgAwIBAgIJAPdWFtOmThF9MA0GCSqGSIb3DQEBCwUAMIGqMQswCQYD
VQQGEwJJVDEOMAwGA1UECAwFSXRhbHkxDjAMBgNVBAcMBU1pbGFuMRkwFwYDVQQK
DBBUZXN0IENBLCBMaW1pdGVkMSMwIQYDVQQLDBpTZXJ2ZXIgUmVzZWFyY2ggRGVw
YXJ0bWVudDESMBAGA1UEAwwJbG9jYWxob3N0MScwJQYJKoZIhvcNAQkBFhhuaWNv
bG8uYmFsemFyb3R0aUBpaXQuaXQwHhcNMTcwOTA3MTEyODIyWhcNMTcxMDA3MTEy
ODIyWjCBqjELMAkGA1UEBhMCSVQxDjAMBgNVBAgMBUl0YWx5MQ4wDAYDVQQHDAVN
aWxhbjEZMBcGA1UECgwQVGVzdCBDQSwgTGltaXRlZDEjMCEGA1UECwwaU2VydmVy
IFJlc2VhcmNoIERlcGFydG1lbnQxEjAQBgNVBAMMCWxvY2FsaG9zdDEnMCUGCSqG
SIb3DQEJARYYbmljb2xvLmJhbHphcm90dGlAaWl0Lml0MIICIjANBgkqhkiG9w0B
AQEFAAOCAg8AMIICCgKCAgEA8DYal/Vn9TnV1kzoQShx2+Z9XOYY7jM6IxOHrjSM
TR+i8+pUESIqrpuk1qDouo9eoG4jO0S8rlLN4S6Bv/ebUCof+QRDiqvJZHzFHkYU
1FC4m6BrNhsY+Vdc0wdJbW5Dtu7ankOeSVZjLLpWSZfjeyjCz4cZocUsAt9rCR9z
pmZoaER9Ur73e/xX1W4r3LhELk5P8UkdQPg9K/FPXUOj77WLl9Iqw4Dxlvkjfjq0
uDBSD4RyeGE/fHrL8PzNh9jOSrp/BsIbZa8SI+2E3YWBYmPuJKNcaMGXboJ2PbmS
gMDG6/cA4aX4L9jTkT6qYxEvQts/pLsM4HbQUC16QQZ9OSZw6JdWCOOisB6ToRnb
WNT9Q2H8d+VkY6CQWidWsuHvdTApPCSQ9tneOmVe/cbkS5VhroKiIc1FTO0zsU4p
sVgmwycUkekGZS64d7M1b21reTexgaIIxW6A73O/7tmYpJ4ZMvcoE85BAtNJeASH
GVOVPTCSoq+/vpJ26ZwJcT3dRqy9eQDiDjx4Ncu9DzEphsRCo3tUUbxDShRwj7YB
QzKeq41oTjAR+7P5KleExk/rV1RYChfK6Si/VnqIdUSBNmF/6rlIxOjnmEwoNEtH
bXxoU2LIzGIHnJABkF94AK9GJY3rubJ+I2HyMPm2glOwQWfQoyPqljQkXwS4GrJZ
KqkCAwEAAaNgMF4wHQYDVR0OBBYEFPDLJc35mKB62HRWgIoq6TvQEp/UMB8GA1Ud
IwQYMBaAFPDLJc35mKB62HRWgIoq6TvQEp/UMA8GA1UdEwEB/wQFMAMBAf8wCwYD
VR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4ICAQCAgt5v3pSMA58eLXBMjq7VWRM2
Fcc7fJ1d/t+xSAJ/XPNjiUPlAJ0nx9C1neiBGJNTZSW9/D1MyXh/eJrXMTPvudXB
IqIWYNf4/+w/nogQGdi3a09YUBM753+pmlGznW3wp2ou8+QvdYKKdpwM7DeqFfo2
eQng1QS+w+OVXYOfKORc1dyxDmgLhrbjhFbDyA8J2befwqShXGQIhyKIH4sEHXGJ
+72iU0VFrnz96peYWEYnC4gzZ3fpPuT4ub3YiuYxtx0/Fx4cMNqyHdqgJONk+2xe
VyyGORzR/iohzJ23V4FUd/DJKCb84+XTwK5FPJuBqthTHClFodOPyv70ILGhx4Wr
TMSEhVNo0PPslJrKdc1VUELsxb/Z+LhYpRhzwQGz6n/Msyzk8pCDWwxDmx9UEk+c
9oQB19Fp3n+z+7gpwXcxgdfCPY9PGhpc7b+Gxb+TWmnXeawA3LMVIfhwr4tvogEw
VwIdghMQgSe4ct5HJPNpijhHfkAroF5DhtC6K/ELyNYM0QzduSttIqXJNmsmE9tn
6tHQV3UDRU0pOnu/yhwdmwRyF5A417Zmhwv1IuS+HDsQ1iv4trcdO6IM6n0FEzDH
QE5zCnqIHvYmitFspSbIrn9RDNa//vYNngJ8D1wIvGIrfbh2AhPyaH7rKZUh6imO
r7N77w0WKlxHbuHV2w==
-----END CERTIFICATE-----"
 ];

  security.pam.services = [
  { name = "i3lock";
    text = ''
      auth required    pam_env.so
      auth sufficient  pam_unix.so try_first_pass likeauth nullok
      auth sufficient  pam_fprintd.so
    '';
    }
  ];
  virtualisation = {
    virtualbox.host.enable = true;
    docker.enable = true;
  };
  services = {
    psd = { # profile sync daemon
            # move browser profile to the tmpfs and sync it back
	    # to speed up and reduce teardonw of hds
      enable = true;
      users = [ "IITaudio" "nixo"];
      # browsers = [ "firefox" "firefox-trunk" "chromium" ]; # empty = all
      resyncTimer = "10m";
    };

    illum.enable = true;

    nginx.enable = true;

    postgresql.enable = true; # required by hydra

    hydra = {
      enable = true;
      hydraURL = "nixo.xyz";
      notificationSender = "nicolo@nixo.xyz";
    };

    gitlab-runner = {
      enable = true;
      packages = [
        pkgs.su   # I think they are needed for every shell build
        pkgs.bash # I think they are needed for every shell build
      ];
      configOptions = {
        concurrent = 1;
	runners = [{
	  name = "lenovo";
	  url = "https://gitlab.iit.it/";
	  token = "afc7ea143a132d3cbf532ad2ede97a";
	  executor = "shell";
	}];
      };
    };

    keybase = { enable = true; };
    kbfs = {
      enable = true;
      mountPoint = "/keybase";
    };


    nginx.virtualHosts."lipa.ms.mff.cuni.cz".locations = let
    path = "/~cunav5am/nix/";
  in {
    ${path}.extraConfig = ''
      rewrite ^${path}(.*)$ http://146.185.144.154/$1 redirect;
      location ~ / { proxy_pass  http://127.0.0.1:8000; }
    '';
  };

     printing = {
       enable = true;
       drivers = [ pkgs.hplip ];
     };

     mysql = {
       enable = true;
       package = pkgs.mariadb;
     };

    fprintd.enable = false;
    udev.extraRules = ''
    # Falcon (does not work)
    SUBSYSTEM=="usb", ACTION=="add", ENV{DEVTYPE}=="usb_device", ATTR{idVendor}=="0403", ATTR{idProduct}=="cb48", MODE="0664", GROUP="input"
    # Teensy boards, http://www.pjrc.com/teensy/
    # The latest version of this file may be found at:
    #   http://www.pjrc.com/teensy/49-teensy.rules
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", ENV{ID_MM_DEVICE_IGNORE}="1"
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789A]?", ENV{MTP_NO_PROBE}="1"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789ABCD]?", MODE:="0666"
    KERNEL=="ttyACM*", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", MODE:="0666"
    '';

    atd.enable = true;
      openssh.enable = true;
      gnome3.gnome-keyring.enable = true;
      locate = {
        enable = true;
        interval = "hourly";
	# FIXME: do not bind to single user
        localuser = "IITaudio";
      };
      # calibre-server =  {
      # 		     enable = true;
      # 		     libraryDir = "/var/lib/calibre";
      # };

      mpd = {
        enable = true;
        # network.host = "127.0.0.1";
        extraConfig = ''
            audio_output {
              type   "pulse"
              name   "Local MPD"
              server "127.0.0.1"
            }
            password "${mpc_password}@read,add,control"
        '';
      };
      # Enable CUPS to print documents.
      # printing.enable = true;
      logind.extraConfig = ''
        HandleLidSwitch=ignore
        HandlePowerKey=ignore
        # HandleSuspendKey=ignore
        HandleHibernateKey=ignore
        HandleLidSwitchDocked=ignore
      '';
#       cron = {
#         enable = true;
#         systemCronJobs = [
#             # example entry
# #            "*/1 * * * * IITaudio ${pkgs.libnotify}/bin/notify-send a b"
#             "5 0 * * * root ${pkgs.s6LinuxUtils}/bin/fstrim -a"
#         ];
#       };
      # Enable the X11 windowing system.
      xserver = {
        wacom.enable = true;
        enable = true;
        layout = "it";
        xkbOptions = "eurosign:e";
        displayManager = {
          # slim = {
          #   theme = pkgs.fetchFromGitHub {
          #     rev = "9a742f2fa6683eeed1e1c0ae1027c90a2c5bc3bb";
          #     owner = "nico202";
          #     repo = "nixos-solarized-slim-theme";
          #     sha256 = "0b86v223qs20ss26q2baf0kx7qn8rihx4ngs609kr9hwg4ad1w8n";
          #   };
          sddm = {
          enable = false;
#           defaultUser = "IITaudio";
          };
          lightdm = {
          enable = false;
          };
        };
        synaptics = {
          enable = true;
          twoFingerScroll = true;
          additionalOptions = ''
            Option "CircularScrolling" "on"
            Option "CircScrollTrigger" "0"
          '';
        };
      };
      syncthing = {
        enable = true;

        user = "IITaudio";
	dataDir = "/home/IITaudio/Syncthing/";
      };
      synergy.server = {
        enable = false;
	address = "127.0.0.1";
      };
  };
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.nixo = {
    isNormalUser = true;
    uid = 1000;
    hashedPassword = "$6$gq.EZMJGcEUz0$Te0mfr9ORf7VM9DvgUFqsJdEFRvKo2PDnOMiyJGnTB3BGI5sW/YDHhs651MJX4WrDC4mRcKz2l7XQSDwpkRJD.";
    extraGroups = [ "wheel" "docker" ];
    shell = "/run/current-system/sw/bin/zsh";
  };


#  users.extraGroups.vboxusers.members = [ "IITaudio" ];
  users.extraUsers.IITaudio = { # audio
    isNormalUser = true;
    uid = 1099;
    extraGroups = [ "audio" # sound card
                    "wheel" # able to become root (sudo) # remove when done
                    "input" # read the mouse
                    "dialout" # read the serial port
#                    "plugdev" # falcon
                    "networkmanager"
                    "vboxusers" # use virtual box
                    "docker" # interact with the docker daemon
                  ];
    hashedPassword = "$6$Qzkwrq8N$wO.e7uPvt.2l9s46srMt/w7hLGflVGHPkRU1lyk2zZ0NS1JcnSFdvLRWI.evRvaiq64/.YGtRa0pb673APTTc1";
    shell = "/run/current-system/sw/bin/zsh";
  };
  programs.nano.nanorc = ''
  set nowrap
  '';
  programs.zsh.interactiveShellInit = ''
  if [[ $UID != 0 ]]; then
    echo '. ${zsh_config}' > $HOME/.zshrc
    ln -sf ${tmux_config} $HOME/.tmux.conf

    mkdir -p $HOME/.i3
    ln -sf ${i3_config} $HOME/.i3/config
    # Copy only if does not exists. Template for enabling mbsync/isync
    cp --no-clobber ${mbsync_config} $HOME/.mbsyncrc
    mkdir -p $HOME/.mpdscribble
    cp --no-clobber ${mpdscribble_config} $HOME/.mpdscribble/mpdscribble.conf

    mkdir -p $HOME/.config/zathura/
    ln -sf ${pkgs.callPackage ./nx/configs/zathura.nix {}} ~/.config/zathura/zathurarc
    mkdir -p ~/.config/polybar
    ln -sf ${pkgs.callPackage ./nx/configs/polybar.nix {}} ~/.config/polybar/config
    mkdir -p $HOME/.pulse
    ln -sf ${pkgs.callPackage ./nx/configs/pulse.nix {}} ~/.pulse/default.pa
  fi
  '';
}
