# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # 3 files to include in new sistems
      ./nx
      ./musnix
      ./myconf.nix
    ];

  # Use the GRUB 2 boot loader.
#  boot.loader.grub.enable = true;
#  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
#  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
#  boot.loader.grub.extraEntries = ''
#    menuentry "Windows 7" {
#      chainloader (hd0,1)+1
#    }
#  '';

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.03";
}
