{pkgs, lib, ...}:

with pkgs; stdenv.mkDerivation rec {
  version = "0.0.1";
  name = "numix_solarized-gtk-theme-${version}";
  src = fetchFromGitHub {
    owner = "Ferdi265";
    repo = "numix-solarized-gtk-theme";
    rev = "b6fce07c95a84dd618ed8594d441d0543192c1bb";
    sha256 = "0gix5gx0n2bqyig3ngc9ky4296kqhnjr2vzjkidsjlkqp8raalmw";
  };
  # should be build dependency, maybe, but as buildDependency it doesn't see sass executable in PATH
  buildInputs = [ gdk_pixbuf glib sass libxml2 ];
  buildPhase = ''
      make DESTDIR=$out
  '';
  prePatch = ''
      sed -i "s/Numix/Numix Solarized/g" index.theme
      export bkgrn=586e75
      export white=eee8d5
      export black=002b36
      sed -i "s/selected_bg_color: #[^\\n]*;/selected_bg_color: #$bkgrn;/g" gtk-3.0/scss/_global.scss
      sed -i "s/selected_bg_color: #[^\\n]*;/selected_bg_color: #$bkgrn;/g" gtk-3.20/scss/_global.scss
      sed -i "s/white: #fff;/white: #$white;/g" gtk-3.0/scss/_global.scss
      sed -i "s/white: #fff;/white: #$white;/g" gtk-3.20/scss/_global.scss
      sed -i "s/black: #000;/black: #$black;/g" gtk-3.0/scss/_global.scss
      sed -i "s/black: #000;/black: #$black;/g" gtk-3.20/scss/_global.scss
      sed -i "s/nselected_bg_color:#[^\\n]*\\n/nselected_bg_color:#$bkgrn\\n/g" gtk-2.0/gtkrc
  '';
  installPhase = ''
    install -dm 755 $out/share/themes/Numix\ Solarized
    cp -dr --no-preserve='ownership' {LICENSE,CREDITS,index.theme,README.md,gtk-2.0,gtk-3.0,gtk-3.20,metacity-1,openbox-3,unity,xfce-notify-4.0,xfwm4} $out/share/themes/Numix\ Solarized/
  '';
  meta = {
    description = "Numix Solarized GTK theme";
    homepage = http://bitterologist.deviantart.com/art/Numix-Solarized-417575928;
    license = stdenv.lib.licenses.gpl3;
    platforms = stdenv.lib.platforms.all;
  };
}
