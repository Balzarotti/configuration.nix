{ stdenv, clang, fetchFromGitHub }:
stdenv.mkDerivation {
  name = "nix-pack.el";
  src = fetchFromGitHub {
    owner = "ardumont";
    repo = "nix-pack";
    rev = "e5134abf066ccf95f59995a7f0dfae43c5c379ba";
    sha256 = "11935hp6ha1yspi60g14lrwipj36fji7x41f79i3as480lv6syd7";
  };
  patchPhase = ''awk -i inplace '/\(use-package/{ print "(require (quote use-package))"}1\' nix-pack.el'';
  installPhase = "mv nix-pack.el $out";
}


