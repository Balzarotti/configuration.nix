{ stdenv }:
stdenv.mkDerivation {
  name = "geomagic-touch";
  src = "/etc/nixos/pkgs/geomagic_touch_device_driver_2016.1-1-amd64.tar.gz";

  buildInputs = [  ];
  installPhase = "mkdir $out; mv usr/* $out/";
}
