{ stdenv }:
stdenv.mkDerivation {
  name = "omni";
  src = "/etc/nixos/pkgs/openhaptics_3.4-0-developer-edition-amd64.tar.gz";

  buildInputs = [  ];
  installPhase = "mkdir $out; mv usr/* $out/";
}
