{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation {
  name = "git-wip";
  src = fetchFromGitHub {
    owner = "bartman";
    repo = "git-wip";
    rev = "c1c9b1a22668f653fe5291be2384dc397f9e0dcc";
    sha256 = "1wyy8k23jfc3n61dl3r0cjdhwk172mz22681by9j9bbqbrf1nxli";
  };
  installPhase = ''
    mkdir -p $out
    #mkdir -p $out/bin
    #mv git-wip $out/bin
    cp -r * $out
  '';
}


