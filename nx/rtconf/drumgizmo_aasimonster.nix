{stdenv, fetchurl, unzip}:

stdenv.mkDerivation rec {
  name = "aasimonster-${version}";
  version = "2.0";

  src = fetchurl {
    url = "http://www.drumgizmo.org/kits/${name}.zip";
    sha256 = "1a86307041d876f170edc3be01af0fa0820fdb6ac7ce56b82cb6a918f12166fd";
  };

  buildInputs = [ unzip ];
  phases = [
    "unpackPhase"
    "installPhase"
  ];

  installPhase = "mkdir $out; mv ./* $out";
}
