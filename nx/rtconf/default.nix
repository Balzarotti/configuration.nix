{ config, pkgs, lib, ... }:

with lib;

let

  cfg = config.nx;

drumgizmo_aasimonster = pkgs.callPackage ./drumgizmo_aasimonster.nix {};

in {
  options.nx = {
    realtime = {
        kernel.enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable all the real-time kernel options";
      };
      packages.enable = mkOption {
        type = types.bool;
        default = false;
        description = "Install music/reatlime packages";
      };
    };
  };

  # Implementation
  config = {
    environment.systemPackages = with pkgs;
      if cfg.realtime.packages.enable
      then [
        # Audio driver
        qjackctl
        jack2Full
        # DAW and recorders
        timemachine
        #    ( pkgs.non.overrideDerivation (oldAttrs: {configurePhase = "python waf configure --prefix=$out --enable-debug ;";
        #      patches = [../patches/non/config.patch];}) )
        non
        # Helpers
        patchage
        # Editors
        mhwaveedit
        audacity
        # Audio analysis
        jaaa
        meterbridge
        # lv2 hosts
        lilv # list lv2 plugins (lv2ls)
        jalv
        # guitarix # depends on insecure webkitgtk
        # developement
        # faust2lv2
        # others
        drumgizmo
        # drumgizmo_aasimonster
        zynaddsubfx
      ]
      else [];

  musnix = { enable = true;
             soundcardPciId = "00:14.0"; # lspci | grep -i audio (USB/firewire)
             # watchdog true: tries to prevent system hangs caused by rt processes
             das_watchdog.enable = false;
             # FIXME: enable interrupt # nixos-option musnix.rtirq
             # rtirq = {
             #   enable = true;
             #	nameList = ""; # driver name
             # };
             kernel = mkIf cfg.realtime.kernel.enable {
               packages = pkgs.linuxPackages_4_8_rt;
               optimize = true;
               realtime = true;
             };
           };
  };
}
