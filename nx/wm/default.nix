{ config, lib, pkgs, ... }:

with lib;

let

  cfg = config.nx.wm;

in {

  options.nx.wm = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Use i3 as wm?
      '';
    };
  };

  # implementation
  config = {
    # Fonts needed by configs/i3.nix
    fonts.fonts = with pkgs;
      if cfg.enable
      then [
        symbola
        font-awesome-ttf
      ]
      else [];

    environment.systemPackages = with pkgs;
      if cfg.enable
      then [
        arandr
        i3status
        i3lock
        dunst rofi
        jshon # i3-workspace script dep
	pcmanfm # file manager
        # xfce.thunar # file manager
        # xfce.thunar_volman
        # xfce.thunar-archive-plugin
        gnome3.gvfs # enable networks and others in thunar
        xfce.gtk_xfce_engine # gtk themes
        xfce.tumbler # files preview
        xfce.exo # open file helper
        shared_mime_info # required for mime file association
        desktop_file_utils
        gnome3.dconf # network-manager-applet
      ]
      else [];

    services.xserver = if cfg.enable
    then {
      windowManager = {
        i3 = {
          enable = true;
          # configFile = (import ../configs/i3.nix;
          # configFile = cfg.configFile;
        };
        default = "i3";
      };
      desktopManager = {
        xterm.enable = true;
        default = "none";
      };
    }
    else {};
  };
}
