{ config, pkgs, ... }:

{
  # Other dependencies
  environment.systemPackages = with pkgs; [
    notmuch
  ];

  programs.zsh.interactiveShellInit = ''
  if [[ $UID != 0 ]]; then
    cp --no-clobber ${pkgs.callPackage ../configs/notmuch.nix {}} $HOME/.notmuch-config
    chmod o+w $HOME/.notmuch-config
    export MAILDIR=~/mails/
    mkdir -p $MAILDIR
  fi
  '';
}
