{stdenv, imagemagick, fetchurl}:

stdenv.mkDerivation rec {
  name = "LockScreen";

  src = fetchurl {
    url = "http://wallpapercave.com/wp/VHH1EMf.jpg";
    sha256 = "0wr26g895h14lm9pqf8gy0cqnz1dxb6m1k28qw014s5a83jx0msl";
  };

  phases = "installPhase";

  buildInputs = [ imagemagick ];

  installPhase = "mkdir $out; convert $src $out/image.png";
}
