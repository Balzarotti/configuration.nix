{ config, pkgs, ... }:

with pkgs;

let
  gtktheme = with pkgs; stdenv.mkDerivation rec {
    version = "0.0.1";
    name = "numix_solarized-gtk-theme-${version}";
    src = fetchFromGitHub {
      owner = "Ferdi265";
      repo = "numix-solarized-gtk-theme";
      rev = "b6fce07c95a84dd618ed8594d441d0543192c1bb";
      sha256 = "0gix5gx0n2bqyig3ngc9ky4296kqhnjr2vzjkidsjlkqp8raalmw";
    };
# should be build dependency, maybe, but as buildDependency it doesn't see sass executable in PATH
    buildInputs = [ gdk_pixbuf glib sass libxml2 ];
    buildPhase = ''
        make DESTDIR=$out
    '';

    prePatch = ''
        sed -i 's/Numix/NumixSolarized/g' index.theme
        # export bkgrn=586e75
        # export white=eee8d5
        # export black=002b36
        # White background to dark
        find ./ -type f -exec sed -i 's/fdf6e3/002b36/g' {} \;
        find ./ -type f -exec sed -i 's/eee8d5/002b36/g' {} \;
        # Dark text to gray
        find ./ -type f -exec sed -i 's/06313b/829396/g' {} \;
        find ./ -type f -exec sed -i 's/002e3a/829396/g' {} \;
        find ./ -type f -exec sed -i 's/073642/829396/g' {} \;
        # Lemon to Yellow
        find ./ -type f -exec sed -i 's/859900/b58900/g' {} \;

        # sed -i 's/gtk.css/gtk-dark.css/;g' gtk-3.20/gtk.css
        # sed -i 's/selected_bg_color: #[^\\n]*;/selected_bg_color: #'$bkgrn';/g' gtk-3.0/scss/_global.scss
        # sed -i 's/selected_bg_color: #[^\\n]*;/selected_bg_color: #'$bkgrn';/g' gtk-3.20/scss/_global.scss
        # sed -i 's/white: #fff;/white: #'$white';/g' gtk-3.0/scss/_global.scss
        # sed -i 's/white: #fff;/white: #'$white';/g' gtk-3.20/scss/_global.scss
        # sed -i 's/black: #000;/black: #'$black';/g' gtk-3.0/scss/_global.scss
        # sed -i 's/black: #000;/black: #'$black';/g' gtk-3.20/scss/_global.scss
        # sed -i 's/nselected_bg_color:#[^\\n]*\\n/nselected_bg_color:#'$bkgrn'\\n/g' gtk-2.0/gtkrc
    '';

    installPhase = ''
      install -dm 755 $out/share/themes/Numix\ Solarized
      cp -dr --no-preserve='ownership' {LICENSE,CREDITS,index.theme,README.md,gtk-2.0,gtk-3.0,gtk-3.20,metacity-1,openbox-3,unity,xfce-notify-4.0,xfwm4} $out/share/themes/Numix\ Solarized/
    '';
    meta = {
      description = "Numix Solarized GTK theme";
      homepage = http://bitterologist.deviantart.com/art/Numix-Solarized-417575928;
      license = stdenv.lib.licenses.gpl3;
      platforms = stdenv.lib.platforms.all;
    };
  };

    icon-theme-name = "Paper";#Numix-Circle
in

{ environment.systemPackages = [
    gnome3.gtk3
    # oxygen-gtk
    librsvg
    # decide which one
    # faba-icon-theme
    paper-icon-theme
    numix-icon-theme
    # moka-icon-theme
    breeze-icons # KDE
    numix-icon-theme-circle
    tango-icon-theme
    hicolor_icon_theme
    gnome3.defaultIconTheme
    gnome2.gnomeicontheme
    xfce.libxfcegui4
    # choose theme?
    lxappearance
    (pkgs.callPackage ./wallpapers/default.nix {})
  ];

  fonts.fonts = with pkgs; [
    carlito # calibri metrics compatible
    caladea # cambria metrics compatible
    siji # Needed by polybar
    fira
    fira-code
    source-code-pro
    google-fonts
    unifont
    inconsolata
    # japanese font
    kochi-substitute

    libertine # biolinum and others
  ];

  environment.shellInit = ''
      export GTK_PATH=$GTK_PATH:${gtktheme}/lib/gtk-2.0
      export GTK_PATH=$GTK_PATH:${gtktheme}/lib/gtk-3.0
      export GTK_PATH=$GTK_PATH:${gtk-engine-murrine}/lib/gtk-2.0
      export GTK_PATH=$GTK_PATH:${gtk-engine-murrine}/lib/gtk-3.0

      ## automount
      export GIO_EXTRA_MODULES=${pkgs.gnome3.gvfs}/lib/gio/modules
      export GTK_DATA_PREFIX=${gtktheme}
      export GTK_THEME="adapta-theme"
      export XCURSOR_PATH=~/.icons:${config.system.path}/share/icons

      export GTK2_RC_FILES=${pkgs.writeText "iconrc" ''gtk-icon-theme-name="${icon-theme-name}"''}
      export GTK2_RC_FILES=$GTK2_RC_FILES:"$(find ${gtktheme} -name gtkrc | head -1 | tr '\n' ' ' | sed -s "s/ $//")"
      export GTK2_RC_FILES=$GTK2_RC_FILES:${writeText "keyrc" ''gtk-key-theme-name = "Emacs"''}
      export GTK2_RC_FILES=$GTK2_RC_FILES:${writeText "keyrc" ''gtk-key-theme-name = "Emacs"''}

      # SVG icon theme
      export GDK_PIXBUF_MODULE_FILE=$(echo ${pkgs.librsvg.out}/lib/gdk-pixbuf-2.0/*/loaders.cache)
  '';
}
