with import <nixpkgs> {};

let

  

in

pkgs.writeScriptBin "" ''
#! /usr/bin/env bash
# Init a git repo in the dest dir
# Then, create a default.nix in the current dir
# The creation should be interactive
if [ "$1" ]; then
    name="$1"
else
    echo "Name not given. Where to create [name]?"
    read -p name "Dest: "
fi
echo "Using name $name"

${pkgs.git}/bin/git init "$name"

# Code that creates the default.nix
# Use defaults to auto add dependencies etc
''


