{writeScriptBin,
xbacklight,
device_path ? "/sys/class/backlight/intel_backlight/",
brightness ? "actual_brightness",
max_brightness ? "max_brightness"}:

writeScriptBin "brightness-control" ''
#!/usr/bin/env bash
xbacklight=${xbacklight}/bin/xbacklight
devicePath=${device_path}
brightness=${brightness}
maxBrightness=${max_brightness}
actualBrightness=$(cat $devicePath$brightness)
maxBrightness=$(cat $devicePath$maxBrightness)

##TODO: allow pecent
change="$1"

if echo $1 | grep -q "+"; then
    $xbacklight -inc 4
    actualBrightness=$(cat $devicePath$brightness)
    custom-notify-send "Value $actualBrigthness" -t 1
fi
if echo $1 | grep -q "-"; then
    $xbacklight -dec 4
    actualBrightness=$(cat $devicePath$brightness)
    custom-notify-send "Value $actualBrigthness" -t 1
fi
''
