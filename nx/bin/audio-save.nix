{writeScriptBin, enchive, ffmpeg}:

writeScriptBin "audio-save" ''
#! /usr/bin/env bash
FORMAT="ogg"
OUTPUTNAME="$(today)_$(now)"
LOCKFILE="/tmp/.audio-record.lock"
OUTPUTDIR="/home/IITaudio/Recordings"
mkdir -p "$OUTPUTDIR"

recStart() {
    echo "Start"
    # redirect to subshell (https://stackoverflow.com/questions/1652680/how-to-get-the-pid-of-a-process-that-is-piped-to-another-process-in-bash#8048493)
    # to get the pid of ffmpeg (and not the one of enchive)
    ${ffmpeg}/bin/ffmpeg -f alsa -ac 2 -ar 44100 -i hw:0 -f $FORMAT - &>/dev/null > >(${enchive}/bin/enchive archive > "$OUTPUTDIR/$OUTPUTNAME.$FORMAT.enchive") &
    echo $! > $LOCKFILE
}

if [ ! -e "$LOCKFILE" ]; then
    recStart
else
    echo "Stop"
    kill $(cat "$LOCKFILE") && SUCCESS=1 || SUCCESS=0
    rm $LOCKFILE
    if [ "$SUCCESS" == "0" ]; then
	echo "Lockfile existed but process not running, starting"
	recStart
    fi
fi
''
