{writeScriptBin, isync, tango-icon-theme, notmuch, libnotify}:

# TODO: daemonify
writeScriptBin "update-mails" ''
#! /usr/bin/env bash
# Based on
# https://raw.githubusercontent.com/natmey/dotfiles/master/notmuch/notmuch-notification.sh

# Download emails from every account
${isync}/bin/mbsync -a

# Update notmuch db
notmuch new

# The notmuch search that will generate subjects you want
SEARCH="tag:unread"
# The number of subjects to show in the notification
#   corresponds to the --limit option of notmuch search
LIMIT=3

STATFILE=/tmp/.last-mail-notification

# the icon in the notification window
# FIXME: use the default icon theme (icon theme global variable?)
NOTIFICATION_ICON="${tango-icon-theme}/share/icons/Tango/24x24/apps/internet-mail.png"

# the sort order of subjects
#   corresponds to the --sort option of notmuch search
SORT="newest-first"

# have notmuch count the number of messages in the search
UNREAD_COUNT=$(${notmuch}/bin/notmuch count --output=messages "$SEARCH")

if [ "$UNREAD_COUNT" -gt 0 ]; then
  # have notmuch pull the specified number of mail subjects from the search.
  # also, do some rought formatting of the result, to pull thread string,
  # sender etc. leaving just the subject text.
  TXT_SUBS=$(${notmuch}/bin/notmuch search --format=text --output=summary --limit="$LIMIT" --sort="$SORT" "$SEARCH" | sed 's/^[^;]*; //' | sed 's/$/\n'/)
  # special characters like @, &, (, ), etc. break notify-send
  # however it likes HTML encoding just fine, so use recode.
#  HTML_SUBS=$(echo "$TXT_SUBS" | recode UTF-8..html)
#  HTML_SUBS=$(echo "$TXT_SUBS" | recode UTF-8..html)
   HTML_SUBS=$TXT_SUBS
   if [ -e $STATFILE ]; then # notify on changes only
     LAST="$(cat $STATFILE)"
   else
     LAST=""
   fi

#   custom-notify-send "Last notification was $LAST"
   if [ "$HTML_SUBS" != "$LAST" ]; then
     DISPLAY=:0.0 ${libnotify}/bin/notify-send -i "$NOTIFICATION_ICON" "$UNREAD_COUNT unread mesages." "$HTML_SUBS"

     echo "$HTML_SUBS" > $STATFILE # Update state
#   else
#      custom-notify-send "same notification"
   fi
elif [ -z "$1" ]; then
  exit 0
elif [ "$1" == "--show-none" ]; then
  DISPLAY=:0.0 ${libnotify}/bin/notify-send -i "$NOTIFICATION_ICON" "No unread messages."
fi

exit 0
''
