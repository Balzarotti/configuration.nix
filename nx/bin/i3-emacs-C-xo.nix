with import <nixpkgs> {};

pkgs.writeScriptBin "" ''
#! /usr/bin/env bash
# Change i3 windows focus like emacs C-x o:
# 1. Move to other window if only 2 are present
# 2. Ask for win number elsewhere?

WINDOWS=$(${pkgs.xdotool} search --all --onlyvisible --desktop $(xprop -notype -root _NET_CURRENT_DESKTOP | cut -c 24-) "" 2>/dev/null)

count=0
printf '%s\n' "$WINDOWS" | while IFS= read -r line
do
    count=$((count + 1))
done

if [ $count -gt 2 ]; then
elif [ $count -le 1 ]; then
    
else
    # just switch to the other one
    # HOW TO KNOW THE DIRECTION!?
    i3-msg focus down
fi

''


