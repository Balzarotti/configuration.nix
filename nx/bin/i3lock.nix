{writeScriptBin, i3lock, xorg, mpc}:

writeScriptBin "custom-i3lock" ''
#! /usr/bin/env bash
##Possible improvements:
#	external pre&post commands

MPC=${mpc}/bin/custom-mpc

lockfile=/tmp/.locked

# Suspend notification system (dunst)
pkill --signal SIGUSR1 dunst
# Suspend hhpc (it auto-unlocks i3lock)
pkill --signal SIGUSR1 hhpc

#Suspend music
paused=0
if $MPC status | grep -q playing
then
    paused=1
    $MPC pause
fi

# Start background recording (if not already running)
RECORDING_STARTED=0
if [ ! $(audio-save running)]; then
   audio-save
   RECORDING_STARTED=1
fi

key-log on

#Notify other programs that the screen is locked
touch $lockfile
#Really lock the screen
${i3lock}/bin/i3lock --tiling --no-unlock-indicator --nofork --show-failed-attempts "$@"
rm $lockfile

# Resume music if was playing
[ $paused -eq 1 ] && $MPC play
# Resume notification system (dunst) (only if not disabled)
[ ! -f /tmp/.presentation-mode ] && pkill --signal SIGUSR2 dunst
# Resume hhpc
pkill --signal SIGUSR2 hhpc

#Wake screen if down
${xorg.xset}/bin/xset dpms force on
# Stop recording (if started)
if [ "$RECORDING_STARTED" == "1" ]; then
   audio-save
fi
key-log off
''
