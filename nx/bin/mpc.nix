{writeScriptBin, mpc_cli, password}:

writeScriptBin "custom-mpc" ''
  #! /usr/bin/env sh
  ${mpc_cli}/bin/mpc -P ${password} "$@"
''
