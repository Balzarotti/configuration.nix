{writeScriptBin, acpi, libnotify}:

writeScriptBin "check-battery-low" ''
#! /usr/bin/env bash

BATTINFO=$(${acpi}/bin/acpi -b)
if [[ $(echo $BATTINFO | grep Discharging) && $(echo $BATTINFO | cut -f 5 -d " ") < 00:30:00 ]]; then
    DISPLAY=:0.0 ${libnotify}/bin/notify-send "low battery" "$BATTINFO"
fi
''
