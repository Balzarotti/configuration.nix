{writeScriptBin, password, rxvt_unicode, ncmpcpp}:

writeScriptBin "custom-ncmpcpp" ''
#! /usr/bin/env sh
${rxvt_unicode}/bin/urxvt -name ncmpcpp -e ${ncmpcpp}/bin/ncmpcpp -h ${password}@localhost "$@"
''
