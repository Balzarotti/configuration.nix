{writeText,
gnupg}:

writeText "notmuch-config" ''
[database]
path=/home/USERNAME/mails
# default: $MAILDIR

[user]
name=Nicolò Balzarotti
primary_email=antothersms@gmail.com
other_email=nicolo.balzarotti@iit.it
# Add to mbsyncrc too
# n.balzarotti@studenti.unisr.it;psymenot@ymail.it;
[new]
tags=unread;inbox;
ignore=
[search]
exclude_tags=deleted;spam;
[maildir]
synchronize_flags=true
[crypto]
gpg_path=${gnupg}/bin/gpg2
''
