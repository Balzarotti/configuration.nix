{i3lock_pkg,
mpc_command,
brightness_control,
modifier ? "Mod4",
touchpad_disable_timeout ? "0.3",
mailTimeout ? "30",
terminal, terminalCommandParam,
fileManager,
saveWinName, saveWinCmd,
writeText,
callPackage,
# packages

tmux, dunst,
go-sct,
feh,
clipit,
blueman,
mpdscribble,
beets,
keybase,
qnotero,
offlineimap,


torbrowser,
rxvt,
rxvt_unicode,

vlc,


acpi,
pythonFull,
thunderbird,

hexchat,
firefox,
deluge,
evince,
bashmount,

zotero,
filezilla,
gnome3,
libreoffice,
gimp,

xorg
}:

writeText "i3config" ''
# TODO: When upgrade to 4.13, move theme to existing Xdefaults/Xresourse variables and load them with
# TODO: set_from_resource $<name> <resource_name> <fallback>
# TODO: read more here: http://build.i3wm.org/docs/userguide.html#xresources
# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

set $mod ${modifier}

new_float normal 0
new_window pixel 1

## default output
#mouse_warping none
mouse_warping output

## The following keeps the titlebar
#new_window normal 1

#default_orientation vertical

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8
#font pango:Fira Sans 8
font pango:Fira Sans 8
# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

bindsym $mod+backslash exec --no-startup-id toggle-guake
#bindsym $mod+Shift+v exec --no-startup-id xdotool mousemove_relative --sync 0 1; exec --no-startup-id xdotool click 1

## Mod1 is Alt

# Start a terminal
# bindsym $mod+Return exec terminator
bindsym $mod+Return exec ${terminal} ${terminalCommandParam} ${tmux}/bin/tmux new
bindsym $mod+Mod1+Return exec ${terminal}
# Start file manager
bindsym $mod+Shift+Return exec ${fileManager}
bindsym $mod+Shift+BackSpace exec --no-startup-id i3-clean-this-workspace

# kill focused window
bindsym $mod+Shift+q exec --no-startup-id ${saveWinCmd}; kill

# start dmenu (a program launcher)
bindsym XF86Launch6 exec custom-rofi -show run
#bindsym $mod+space exec custom-rofi -show run
#bindsym 24  exec custom-rofi -show run
bindsym $mod+space exec custom-rofi -show run

# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
# vim way
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+ograve focus right
# change focus - emacs like
# bindsym $mod+p focus up
bindsym $mod+n focus down
bindsym $mod+b focus left
## bindsym $mod+f focus right
bindsym $mod+a focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

#mode "emacsLikeWindows" {
#     bindsym o focus left; mode "default"
#}
# emacs like mode
#bindsym $mod+x mode "emacsLikeWindows"

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+ograve move right

# move focused window - emacs like
##bindsym $mod+Shift+f move right
#bindsym $mod+Shift+n move down
#bindsym $mod+Shift+p move up
#bindsym $mod+Shift+ move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for then focused container
bindsym $mod+f fullscreen toggle
bindsym $mod+Shift+f bar mode toggle
#Useless? Disable maybe
bindsym $mod+g fullscreen global

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
# not working on lenovo?
# bindsym $mod+ISO_Level3_Shift+space focus mode_toggle
#bindsym $mod+Shift+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent # use as emacs mod+f
bindsym $mod+u focus parent

# focus the child container
#bindsym $mod+d focus child

set $EXTERNAL "0"
# Frozen apps moved to "0"
set $workspace1 "1: "
set $workspace2 "2: "
set $workspace3 "3: "
set $workspace4 "4: "
set $workspace5 "5: "
set $workspace6 "6: "
set $workspace7 "7: "
set $workspace8 "8: "
set $workspace9 "9: "
set $workspace10 "10: "
set $workspace0 "0: "
set $waitlist " "

# Switch to default workspace
exec --no-startup-id i3-msg workspace $workspace1

# switch to workspace
bindsym $mod+F1 workspace $workspace0
bindsym $mod+1 workspace $workspace1
bindcode 248 exec --no-startup-id toggle-workspaces 1
#bindcode $mod+248 workspace $workspace9
#bindcode 248 exec --no-startup-id switch-workspace $workspace1 $workspace9
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+Menu workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+XF86WebCam workspace $workspace8
bindsym $mod+9 workspace $workspace9

bindsym $mod+0 workspace $workspace10
bindsym $mod+End workspace $waitlist
# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+Menu move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9
bindsym $mod+Shift+0 move container to workspace $workspace10
#Difficult to reach, just to hide unwanted-sleeping processes
bindsym $mod+Shift+End move container to workspace $waitlist

assign [class="Firefox"] $workspace1
assign [class="Emacs"] $workspace3
assign [class="Filezilla"] $workspace4
assign [class="libreofficedev"] $workspace5
assign [class="libprs500"] $workspace5
assign [class="libreofficedev-writer"] $workspace5
assign [class="GParted"] $workspace6
assign [class="Gimp"] $workspace8
assign [class="Inkscape"] $workspace8
assign [class="Vlc"] $workspace9
assign [class="Ario"] $workspace10
assign [class=".meld-wrapped"] $workspace3
assign [class="Brasero"] $workspace10

#Music production: $workspace6 $workspace7
assign [class="Non-Mixer"] → $workspace6
assign [class="Non-Timeline"] → $workspace7
assign [class="Hydrogen"] → $workspace7
assign [class="Zynaddsubfx"] → $workspace6
assign [class="Non-Session-Manager"] → $workspace6
assign [class="Non-Sequencer"] → $workspace7

#Music production
for_window [class="Zynaddsubfx"] floating disabled
for_window [title=".*Mixer.*" class="Hydrogen"] move to workspace $workspace6
# Other
#for_window [class="Main.py" instance="guake"] floating enable
for_window [instance="gtk-recordmydesktop"] floating enable
for_window [class="gtk-recordmydesktop"] floating enable

hide_edge_borders both

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
#bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

bindsym $mod+Shift+e exec [ "`echo -e "My fault: continue using!\n Exit" | custom-rofi -dmenu -p "End your X Session (kill i3)?"`" == " Exit" ] && i3-msg exit

mode "paste" { #Special mode to copy output of commands to clipboard
     bindsym d exec --no-startup-id copy-command-to-clipboard today; mode default
     bindsym Shift+I exec --no-startup-id copy-command-to-clipboard myself iban; mode default
     bindsym e exec --no-startup-id copy-command-to-clipboard myself email; mode default
     bindsym f exec --no-startup-id copy-command-to-clipboard myself CF; mode default
     bindsym i exec --no-startup-id copy-command-to-clipboard my-local-ip; mode default
     bindsym t exec --no-startup-id copy-command-to-clipboard now; mode default
     bindsym > exec --no-startup-id copy-command-to-clipboard echo "→"; mode default
     bindsym < exec --no-startup-id copy-command-to-clipboard echo "←"; mode default
     bindsym Escape mode default
}

bindsym $mod+Shift+d mode "paste"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym ograve resize grow width 10 px or 10 ppt

        bindsym b resize shrink width 10 px or 10 ppt
        bindsym n resize grow height 10 px or 10 ppt
        bindsym p resize shrink height 10 px or 10 ppt
        bindsym f resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"

	# emacs like enter
        bindsym $mod+m mode "default"
        bindsym Control+m mode "default"
        bindsym $mod+j mode "default"
        bindsym Control+j mode "default"
        bindsym $mod+o mode "default"
        bindsym Control+o mode "default"

}


# # Start i3bar to display a workspace bar (plus the system information i3status
# # finds out, if available)
# bar {
#         status_command i3status -c ${callPackage ./i3status.nix {}}
# 	position top
# 	tray_output eDP1

#         colors {
#                 background #002b36
#                 statusline #839496
#                 separator  #586e75
#                 focused_workspace  #b58900 #b58900 #002b36
#                 active_workspace   #586e75 #586e75 #002b36
#                 inactive_workspace #073642 #002b36 #839496
#                 urgent_workspace   #dc322f #dc322f #fdf6e3
#         }
# }

client.focused          #002b36 #586e75 #fdf6e3 #268bd2
client.focused_inactive #002b36 #073642 #839496 #073642
client.unfocused        #002b36 #073642 #839496 #073642
client.urgent           #002b36 #dc322f #fdf6e3 #002b36

# Startup programs
exec ${xorg.xrdb}/bin/xrdb -merge ${callPackage ./Xdefaults.nix {}}
exec ${dunst}/bin/dunst -config ${callPackage ./dunst.nix {}}
# redshift-like
exec --no-startup-id ${go-sct}/bin/sct

# debug
exec urxvt -e keep-updating-mails
exec --no-startup-id keep-updating-mails
exec_always --no-startup-id custom-polybar

exec_always --no-startup-id ${feh}/bin/feh --bg-scale --randomize ~/Immagini/Wallpaper/
## this prevents nix-shell from starting tmux server
exec --no-sturtup-id ${tmux}/bin/tmux new -d
exec --no-sturtup-id ${xorg.xf86inputsynaptics}/bin/syndaemon -i ${touchpad_disable_timeout} -d -K
#exec --no-startup-id wicd-gtk -t
# TODO: add preset save (auto-load saved, else ask)
exec --no-startup-id externalmonitor-connected && monitor-config
exec --no-startup-id nm-applet
# exec --no-startup-id compton -f -D 3
exec --no-startup-id ${clipit}/bin/clipit
# only if bluetooth enabled in config (add config XD)
exec --no-startup-id ${blueman}/bin/blueman-applet
exec --no-startup-id emacs --daemon
exec --no-startup-id ${mpdscribble}/bin/mpdscribble
exec --no-startup-id ${beets}/bin/beet web
#exec --no-startup-id custom-guake
# Freezes the system
# exec --no-startup-id mkdir -p ${keybase}/bin/keybase; kbfsfuse ./keybase
exec --no-startup-id ${qnotero}/bin/qnotero
# FIXME: add config file
exec --no-startup-id keep-running ${offlineimap}/bin/offlineimap
#exec --no-startup-id keep-checking-internet
# exec --no-startup-id telegram-daemon
# exec --no-startup-id hhpc /bin/hhpc
# Start with vlc/mplayer etc and kill afterwards
# exec --no-startup-id hhpc

#exec --no-startup-id touchegg #Touchscreen. Find a way to have this on ASUS only
#guake variables (needed by various commands) #Also, disable for tocuhpad
set $telegram_tab 0
#off if you want to delete old temp files
#exec --no-startup-id restore-old-toggles on

#Custom keybindings
bindcode 160 exec systemctl suspend
bindsym $mod+r mode "resize"

#Screenshots : move not working!? :(
set $moveCommand "mv \$f $HOME/Immagini/Screenshots/"
bindsym Print exec scrot --no-startup-id -q 100 '%Y-%m-%d-%T_$wx$h_scrot.png' -e $moveCommand
bindsym --release $mod+Print exec scrot --no-startup-id -q 100 -s '%Y-%m-%d-%T_$wx$h_scrot_selection.png' -e $moveCommand
bindsym --release $mod+Shift+Print exec scrot --no-startup-id -q 100 -s -e 'cat $f | xclip -selection c -t image/png' -e $moveCommand
bindsym $mod+Ctrl+Shift+Print exec scrot --no-startup-id -q 100 -u '%Y-%m-%d-%T_$wx$h_scrot.png' -e $moveCommand

# Run a custom command, that does not require to change the config (situation-dependent?)
bindsym $mod+c exec --no-startup-id $HOME/bin/temporary-action

bindsym $mod+Shift+a --release exec --no-startup-id set-to-workspace

# nix-shell projects:
bindsym $mod+Shift+O exec open-project

# desktop-sensitive open
bindsym $mod+o exec open-workspace-default

#Firefox search clipboard:
bindsym $mod+Shift+S exec ${firefox}/bin/firefox --search $(xclip -o); workspace $workspace1
bindsym $mod+Shift+P exec ${torbrowser}/bin/tor-browser
#firefox --private-window

#Firefox search rofi
#FIXME: script to allow other search engine/cancel
#TODO: add also a now-playing lyric!
bindsym $mod+Shift+W exec firefox --new-tab "www.google.it/search?q=`echo -e "Cancel" | custom-rofi -dmenu -p "Search on google.it: "`"; workspace $workspace1

#Presentation mode: pause dunst notification (& others?)
#Mute mode: pause notification sounds (custom-notify-send)

bindsym $mod+F8 exec --no-startup-id toggle-presentation
bindsym $mod+F10 exec --no-startup-id toggle-mute-mode

#Wicd
# bindsym $mod+F2 exec wicd-gtk --no-tray

bindsym $mod+Ctrl+Delete exec rxvt_unicode -e htop

#Enable output monitor (opens config choose/edit)
bindsym XF86Display exec --no-startup-id monitor-config
# Lenovo E31 needs this
bindsym $mod+p exec --no-startup-id monitor-config

#Move workspace to other monitor; use both LR and updown shift to be sure to cover all cases
bindsym $mod+Shift+F8 move workspace to output left; move workspace to output down

# replaced by illum system service (services.illum.enable = true;)
# bindsym XF86MonBrightnessUp exec --no-startup-id ${brightness_control} +5
# bindsym XF86MonBrightnessDown exec --no-startup-id ${brightness_control} -5

set $lockwall "${callPackage ../style/wallpapers {}}/image.png"
bindsym $mod+Escape exec --no-startup-id ${i3lock_pkg} --image=$lockwall

bindsym XF86AudioRaiseVolume exec --no-startup-id amixer sset 'Master' 5%+ unmute
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -q sset 'Master' 5%- unmute
bindsym XF86AudioMute exec --no-startup-id amixer -q sset 'Master' toggle
# custom-mpc toggle /stop/next/prev replace when adding bin to nixos
bindsym XF86AudioPlay exec --no-startup-id ${mpc_command} toggle
bindsym XF86AudioStop exec --no-startup-id ${mpc_command} stop
bindsym XF86AudioNext exec --no-startup-id ${mpc_command} next
bindsym XF86AudioPrev exec --no-startup-id ${mpc_command} prev
# Dependency: ${vlc}
bindsym $mod+XF86AudioPlay exec --no-startup-id player-control ${vlc}/bin/vlc toggle
bindsym $mod+XF86AudioStop exec --no-startup-id player-control ${vlc}/bin/vlc stop
bindsym $mod+XF86AudioNext exec --no-startup-id player-control ${vlc}/bin/vlc next
bindsym $mod+XF86AudioPrev exec --no-startup-id player-control ${vlc}/bin/vlc prev

bindsym XF86TouchpadToggle exec --no-startup-id toggle-touchpad
bindsym XF86WebCam exec ${gimp}/bin/gimp

bindsym $mod+F9 exec --no-startup-id toggle-touchscreen
bindsym $mod+Shift+minus exec --no-startup-id is-playing && custom-notify-send -t 2000 "Now playing: " "$(custom-mpc current --format '%artist% - %title%')" || custom-notify-send -t 2000 "Paused: " "$(custom-mpc current --format '%artist% - %title%')"
bindsym $mod+Shift+colon exec --no-startup-id custom-notify-send -t 2000 "It's $(now)"
bindsym $mod+Shift+semicolon exec --no-startup-id custom-notify-send -t 3000 Battery "$(${acpi}/bin/acpi -b)"
#bindsym $mod+§ exec --no-startup-id new-mail-notify --show-none

# Dependency: ${rxvt_unicode}
# Dependency: ${pythonFull}
bindsym XF86Calculator exec ${terminal} -e python
#bindsym XF86Calculator exec ${terminal} -e "python -i -c 'import numpy as np'"


#Intercept poweroff, ask if sure
#don't know how to poweroff. Not working intercept
#Replace with a mode?
bindsym XF86PowerOff exec --no-startup-id poweroff-pressed
## XF86Sleep is exec after wake from sleep
#bindsym XF86Sleep exec --no-startup-id poweroff-pressed "Sleep"

# Launch mode, one app per key.
mode "launch" {
    bindsym t exec ${thunderbird}/bin/thunderbird ; mode "default"
    bindsym f exec ${filezilla}/bin/filezilla ; mode "default"
    # bindsym z exec zotero/bin/zotero; mode "default"
    bindsym h exec ${hexchat}/bin/hexchat ; mode "default"
    bindsym w exec ${firefox}/bin/firefox ; mode "default"
    bindsym 2 exec ${firefox}/bin/firefox -p; mode "default"
    bindsym d exec ${deluge}/bin/deluge ; mode "default"
    bindsym e exec emacsclient -a emacs -c -n; mode "default"
    bindsym p exec ${evince}/bin/evince; mode "default"
    bindsym m exec ${bashmount}/bin/bashmount; mode "default"
    bindsym g exec ${gimp}/bin/gimp ; mode "default"
    bindsym r exec ${gnome3.file-roller}/bin/file-roller ; mode "default"
    bindsym l exec ${libreoffice}/bin/libreoffice ; mode "default"
    bindsym v exec --no-startup-id ${vlc}/bin/vlc ; mode "default"
    bindsym F2 exec wicd-gtk --no-tray ; mode "default"
    bindsym c exec ${gnome3.california}/bin/california; mode "default"
#    bindsym backslash exec --no-startup-id toggle-guake; mode "default"
#    bindsym $mod+backslash exec --no-startup-id toggle-guake; mode "default"
    # gnome-disks will not launch twice, gparted will. Avoid that.
#    bindsym d workspace 7; exec ((pidof gpartedbin) || gksudo gparted); exec gnome-disks ; mode "default"

    bindsym Escape mode "default"
}
bindsym $mod+Tab mode "launch"

#set $chat_with_peer "chat_with_peer 1 Paola, 2 Enrico, 3 Band, 4 Al"
#mode $chat_with_peer {
#     (mode set back to default by the script. Right?)
#     #Chat peer selection
#     bindsym 1 exec --no-startup-id chat-with-peer "Paola_Del_sette"
#     bindsym 2 exec --no-startup-id chat-with-peer "Enrico"
#     bindsym 3 exec --no-startup-id chat-with-peer "Unicorn_Flakes_A81"
#     bindsym 4 exec --no-startup-id chat-with-peer "Francesca_Conca"
#     bindsym Escape exec --no-startup-id toggle-guake; mode "default"
#}

# FIXME: re-enable with guake
#Chat-with-peer mode
#bindsym $mod+Shift+h exec --no-startup-id telegram-action '/history' --no-quit
#bindsym $mod+Shift+n exec --no-startup-id latest-screenshots
#bindsym $mod+Shift+m exec --no-startup-id mailclient

# undo close window! (run the command in /tmp/.last-win-cmd, security concerns? yep
bindsym $mod+Shift+t exec --no-startup-id $(cat /tmp/.last-win-cmd)

set $mode_system System (l)ock, (e)xit, (s)uspend, (h)ibernate, (r)eboot, (p)oweroff
mode "$mode_system" {
    bindsym l exec --no-startup-id $lockcmd, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id systemctl suspend, mode "default"
    bindsym h exec --no-startup-id systemctl hibernate, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym p exec --no-startup-id systemctl poweroff, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Pause mode "$mode_system"

## Freeze/unfreeze windows

mode "un/freeze" {
     bindsym f exec --no-startup-id un-freeze-window; mode "default"
     bindsym u exec --no-startup-id un-freeze-window unfreeze; mode "default"
     bindsym Escape mode "default"
     bindsym q mode "default"
}
bindsym $mod+Shift+F1 mode "un/freeze"

#This lines added automatically with bin/set-to-workspace
assign [class="Baobab"] → $workspace4
assign [class="K3b"] → $workspace4
assign [class="Deluge"] → $workspace4
assign [class="Thunderbird"] → $workspace1
assign [class="Soffice"] → $workspace5
assign [class="Pinta"] → $workspace8
assign [class="Gitg"] → $workspace3
assign [class="libreofficedev-impress"] → $workspace5
assign [class="LibreOfficeDev 5.1"] → $workspace5
assign [class="Pavucontrol"] → $workspace6
assign [class="Gazeb"] → $workspace8
assign [class="Zotero"] → $workspace5
assign [class="tufts-vue-VUE"] → $workspace8
assign [class=".k3b-wrapped"] → $workspace4
assign [class="Hexchat"] → $workspace1
assign [class="chromium-browser"] → $workspace9

# assign [class="Zathura"] → $workspace5 ## Disabled
## Does not work
##assign [class="Uget-gtk" instance="uGet"] → $workspace4
assign [class="Zathura"] → $workspace5
assign [class="Simplescreenrecorder"] → $workspace6
assign [class="Blueman-manager"] → $workspace6
assign [class="Skype"] → $workspace7
assign [class="Mumble"] → $workspace7
assign [class="com-eteks-sweethome3d-SweetHome3DBootstrap"] → $workspace8
assign [class="Pencil"] → $workspace8
assign [class="R_x11"] → $workspace8
assign [class="Gimp-2.8"] → $workspace8
assign [class="Gimp"] → $workspace8
for_window [class="Gimp-2.8"] move to workspace $workspace8
for_window [class="Gimp"] move to workspace $workspace8
assign [title="p-mode"] → p-mode
assign [class="Jalv.gtk"] → $workspace6
assign [class="Toplevel"] → $workspace8
assign [class=".zathura-wrapped"] → $workspace5
assign [class="URxvt" instance="ncmpcpp"] → $workspace10
assign [class="CHAI3D"] → $workspace9
assign [class="okular"] → $workspace5

# NEWASSIGNPLACEHOLDER
''
