{writeText}:

writeText "tmux.conf" ''
# set-option -g default-shell $HOME/.nix-profile/bin/zsh
#set-hook -g -n 'after-new-window' 'run "notify-send new-window..."'

#set -g mouse-mode on
set-option -g mouse on

set-option -g history-limit 5000

unbind C-b
set -g prefix C-a
bind C-a send-prefix

# Easy config reload
bind R source-file ~/.tmux.conf \; display-message "tmux.conf reloaded."

# Easy split pane commands
bind = split-window -h
bind - split-window -v
bind M-Enter split-window -h -c "#{pane_current_path}"
bind Enter split-window -v -c "#{pane_current_path}"
unbind '"'
unbind %
bind q killp

# --- colors (solarized dark)
# default statusbar colors
set -g status-bg black
set -g status-fg yellow
set -g status-attr default

# default window title colors
setw -g window-status-fg brightblue
setw -g window-status-bg default

# active window title colors
setw -g window-status-current-fg yellow
setw -g window-status-current-bg default
setw -g window-status-current-attr dim

# pane border
set -g pane-border-fg black
set -g pane-border-bg default
set -g pane-active-border-fg yellow
set -g pane-active-border-bg default

# command line/message text
set -g message-bg black
set -g message-fg yellow

# pane number display
set -g display-panes-active-colour yellow
set -g display-panes-colour brightblue

# clock
setw -g clock-mode-colour yellow
# --- end colors

set -g default-terminal screen-256color

# needed to use tmux inside a nix-shell

set-option -ga update-environment ' PKG_CONFIG_PATH NIX_CFLAGS_COMPILE nativeBuildInput NIX_LDFLAGS CMAKE_INCLUDE_PATH CMAKE_LIBRARY_PATH CMAKE_PREFIX_PATH CONFIG_SHELL CXX DESKTOP_STARTUP_ID NAME LD_LIBRARY_PATH JULIA_PKGDIR BOOST_ROOT BOOST_LIBRARYDIR'
''
