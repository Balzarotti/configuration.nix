{writeText}:

writeText "mbsyncrc" ''

# ACCOUNT INFORMATION
# GMAIL
IMAPAccount gmail
Host imap.gmail.com
User anothersms@gmail.com
# If disabled, password it's asked to the user
# Pass PASSWORD_GMAIL
Port 993
AuthMechs LOGIN
SSLType IMAPS
SSLVersions TLSv1.2
CertificateFile /etc/ssl/certs/ca-certificates.crt

# IIT
IMAPAccount iit
Host mail.iit.it
User nicolo.balzarotti@iit.it
# Pass PASSWORD_IIT
Port 143
AuthMechs LOGIN
SSLType STARTTLS
SSLVersions SSLv3
CertificateFile /etc/ssl/certs/ca-certificates.crt

# THEN WE SPECIFY THE LOCAL AND REMOTE STORAGE
# - THE REMOTE STORAGE IS WHERE WE GET THE MAIL FROM (E.G., THE
#   SPECIFICATION OF AN IMAP ACCOUNT)
# - THE LOCAL STORAGE IS WHERE WE STORE THE EMAIL ON OUR COMPUTER

# REMOTE STORAGE (USE THE IMAP ACCOUNT SPECIFIED ABOVE)
IMAPStore gmail-remote
Account gmail

IMAPStore iit-remote
Account iit

# LOCAL STORAGE (CREATE DIRECTORIES with mkdir -p Maildir/gmail)
MaildirStore gmail-local
Path ~/mails/
Inbox ~/mails/gmail

MaildirStore iit-local
Path ~/mails/
Inbox ~/mails/iit

# CONNECTIONS SPECIFY LINKS BETWEEN REMOTE AND LOCAL FOLDERS
#
# CONNECTIONS ARE SPECIFIED USING PATTERNS, WHICH MATCH REMOTE MAIl
# FOLDERS. SOME COMMONLY USED PATTERS INCLUDE:
#
# 1 "*" TO MATCH EVERYTHING
# 2 "!DIR" TO EXCLUDE "DIR"
# 3 "DIR" TO MATCH DIR

Channel gmail-inbox
Master :gmail-remote:
Slave :gmail-local:
Patterns "INBOX"
Create Both
Expunge Both
SyncState *

Channel gmail-trash
# Bin in english gmail
Master :gmail-remote:"[Gmail]/Cestino"
Slave :gmail-local:"[Gmail].Bin"
Create Both
Expunge Both
SyncState *

Channel gmail-sent
# Sent Mail
Master :gmail-remote:"[Gmail]/Sent"
# Sent Mail
Slave :gmail-local:"[Gmail].Sent Mail"
Create Both
Expunge Both
SyncState *

Channel gmail-all
# All Mail
Master :gmail-remote:"[Gmail]/Tutti i Messaggi"
Slave :gmail-local:"[Gmail].All Mail"
Create Both
Expunge Both
SyncState *

Channel gmail-archive
# All Mail
Master :gmail-remote:"[Gmail]/Archives"
Slave :gmail-local:"[Gmail].Archives"
Create Both
Expunge Both
SyncState *

Channel gmail-starred
# Starred
Master :gmail-remote:"[Gmail]/Speciali"
Slave :gmail-local:"[Gmail].Starred"
Create Both
Expunge Both
SyncState *

# GROUPS PUT TOGETHER CHANNELS, SO THAT WE CAN INVOKE
# MBSYNC ON A GROUP TO SYNC ALL CHANNELS

Group gmail
Channel gmail-trash
Channel gmail-starred
Channel gmail-inbox
Channel gmail-sent
Channel gmail-archives
Channel gmail-all

Channel iit-inbox
Master :iit-remote:
Slave :iit-local:
Patterns "INBOX"
Create Both
Expunge Both
SyncState *

Channel iit-sent
Master :iit-remote:"Sent"
Slave :iit-local:"Sent"
Create Both
Expunge Both
SyncState *

Group iit
Channel iit-inbox
Channel iit-sent
''
