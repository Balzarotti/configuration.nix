# FIXME: add to nixOS

{writeText}:

writeText "nix-pack.el" ''
(defun nix-pack/add-package-directory (dir)
  "A function to add to PACKAGE-DIRECOTR-LIST if the DIR exists."
  (when (file-exists-p (expand-file-name dir))
    (add-to-list #'package-directory-list dir)))

(mapc #'nix-pack/add-package-directory '("/run/current-system/sw/share/emacs/site-lisp/elpa"
        "/run/current-system/sw/share/emacs/site-lisp"
        "~/.nix-profile/share/emacs/site-lisp/elpa"
        "~/.nix-profile/share/emacs/site-lisp"))


(provide 'nix-pack)
''
