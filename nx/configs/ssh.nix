# FIXME: add to nixOS
{writeText}:

writeText "config" ''
# Keepalive
Host *
        ServerAliveInterval 120
# raspi in both homes
Host raspi
	HostName 192.168.1.14
	User pi
# remoteraspi (home 1)
Host remoteraspi
	HostName notify.homepc.it
	User pi
# Needs to be static everywhere somehow
Host G3
	HostName 192.168.1.95
	User pi
	Port 8022
# Both chips (both homes)
Host chip
	HostName 192.168.1.108
	User chip
''
