{writeText,
rxvt_unicode-with-plugins,
firefox
}:

writeText "Xdefaults" ''

! ------------------------------------------------------------------------------
! ROFI Color theme
! ------------------------------------------------------------------------------
rofi.color-enabled: true
rofi.color-window: #002b37, #002b37, #003642
rofi.color-normal: #002b37, #819396, #002b37, #003642, #819396
rofi.color-active: #002b37, #008ed4, #002b37, #003642, #008ed4
rofi.color-urgent: #002b37, #da4281, #002b37, #003642, #da4281
rofi.ssh-client:   ssh
rofi.ssh-command:  {terminal} -e {ssh-client} {host} -t tmux new


URxvt.intensityStyles: false
! urxvt
URxvt*buffered: true
URxvt*cursorBlink: true
URxvt*underlineColor: yellow
URxvt*depth: 32
URxvt*borderless: 1
URxvt*scrollBar: false
URxvt*loginShell: true
Urxvt*secondaryScroll:  true    # Enable Shift-PageUp/Down in screen
URxvt*saveLines: 5000
URxvt*termName: rxvt-unicode
URxvt.perl-ext-common: default,matcher
URxvt.urlLauncher: firefox
URxvt.matcher.button: 1
URxvt.matcher.rend.0: Uline Bold fg5
URxvt.colorUL: #4682B4
! keyboard-select
URxvt.keysym.M-s: perl:keyboard-select:activate
URxvt.keysym.M-r: perl:keyboard-select:search

! `font-size` plugin
URxvt.keysym.C-minus: perl:font-size:decrease
URxvt.keysym.C-equal: perl:font-size:increase
! The default font increase step size is 1. This is for portability across all
! fonts (some fonts do not support particular font sizes). Because changing font
! size only really makes sense for anti-aliased fonts (Terminus is bitmapped),
! and because anti-aliased fonts support any size, it makes sense to just use a
! size step of 4 for faster transitions between font size changes.
URxvt.font-size.step: 4

*background:            #002b36
*foreground:            #c5cfd3
*cursorColor:           #93a1a1
*pointerColorBackground:#586e75
*pointerColorForeground:#93a1a1

!! black dark/light
*color0:                #073642
*color8:                #002b36

!! red dark/light
*color1:                #dc322f
*color9:                #cb4b16

!! green dark/light
*color2:                #859900
*color10:               #586e75

!! yellow dark/light
*color3:                #b58900
*color11:               #657b83

!! blue dark/light
*color4:                #268bd2
*color12:               #839496

!! magenta dark/light
*color5:                #d33682
*color13:               #6c71c4

!! cyan dark/light
*color6:                #2aa198
*color14:               #93a1a1

!! white dark/light
*color7:                #eee8d5
*color15: #fdf6e3

! General
!urxvt*termName:  rxvt-256color
urxvt*loginShell:         true
urxvt*scrollBar:         false
urxvt*secondaryScroll:    true
urxvt*saveLines:         65535
urxvt*cursorBlink:       true
urxvt*urgentOnBell:       true
!urxvt*override-redirect:false
!urxvt*borderLess:       false
!urxvt*internalBorder:       0
!urxvt*externalBorder:       0

! Extensions
urxvt*perl-lib:        ${rxvt_unicode-with-plugins}//lib/urxvt/perl/
urxvt*perl-ext-common: default,matcher,searchable-scrollback
urxvt*urlLauncher:     ${firefox}/bin/firefox
urxvt*matcher.button:  1

/* fonts */
URxvt.font:         xft:Hack-Regular:pixelsize=11,xft:Symbola:pixelsize=9:antialias=true
URxvt.boldFont:     xft:Hack-Bold:pixelsize=11:weight=bold,xft:Symbola:pixelsize=9:antialias=true
URxvt.italicFont:   xft:Hack-RegularOblique:pixelsize=11:slant=italic,xft:Symbola:pixelsize=9:antialias=true
URxvt*letterSpace:  0

!  - catch ugly URLs
urxvt.cutchars:        "`()'*<>[]{|}"
! FIXME: global variable
! Emacs.font: Hack-9
''
