{writeText,
username, mpc_password}:
# Fixes mpd+pulse
writeText "mpdscribble.conf" ''
[mpdscribble]
log = $HOME/.mpdscribble/mpdscribble.log
verbose = 2
host = ${mpc_password}@localhost

[libre.fm]
url = http://turtle.libre.fm/
username = nico202
#echo -n "password" | md5sum | cut -f 1 -d " "
password = INSERT_ME
journal = $HOME/.mpdscribble/librefm.journal
''
