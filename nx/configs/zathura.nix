{writeText}:

writeText "zathurarc" ''
set recolor true
set recolor-darkcolor "#839496"
set recolor-lightcolor "#002b36"
set window-title-basename "true"
set selection-clipboard "clipboard"
''
