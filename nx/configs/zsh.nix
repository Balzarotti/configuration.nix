{writeText,
oh-my-zsh,
torsocks,
now}:

writeText "zshrc" ''

# Fix zsh + emacs + tramp
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

# Installation path
export ZSH=${oh-my-zsh}/share/oh-my-zsh
#export ZSH_CUSTOM=$HOME/.zshcustom
#mkdir -p $ZSH_CUSTOM/plugins

plugins=(git adb systemadmin nix
         tmux torrent web-search
         colored-man-pages)

FIX_THEME_PATH="../../../../../.."
ZSH_THEME="$FIX_THEME_PATH/nix/store/$(basename ${import ./zsh-theme.nix} .zsh-theme)"

# ZSH Config
HYPHEN_INSENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
ENABLE_CORRECTION="ture"
COMPLETITION_WAITING_DOTS="true"

source $ZSH/oh-my-zsh.sh

setopt auto_cd

# Various fixes
if [ "$TMUX" '==' "" ]; then
    export TERM=rxvt-256color
else
    export TERM=screen-256color
fi
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_CONNECTION" ]; then
    export DISPLAY=:0
fi

# privacy mode
if [ -f /tmp/.p-mode ]; then
    # run all command under tor
    ${torsocks}/bin/torsocks on
fi

# Aliases
alias emacs="emacsclient -c -n -a emacs"
alias 'emacs-nw=emacsclient -c -nw -a emacs'
# who needs nano anymore? Just my hands
alias 'nano=emacsclient -nw -a emacs'
alias 'now=${now}'

# replace ls with exa, if available (the check is needed for nix-shell --pure)
if type exa > /dev/null; then
 alias l='exa --all --header --long --color-scale'
 alias lt='exa --all --header --long --tree --color-scale --ignore-glob .git'
 alias ls='exa'
else
 alias l='ls -lah'
 alias lt='ls -lah'
fi

alias julia='julia --color=yes'

# Key binding
bindkey '^ ' autosuggest-accept
bindkey '^[^[[C' forward-word

# Fix tmuxp
export DISABLE_AUTO_TITLE="true"

# https://theshortlog.com/2016/02/11/ipython-nix/
# use with ipython-nix packages, like: ipython-nix numpy
function ipython-nix {
    packages=""
    for arg in $@; do
        packages="$packages $arg"
    done

    nix-shell -p "with pythonPackages; [ ipython $packages ]" --command ipython
}
''
