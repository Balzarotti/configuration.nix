{ config, pkgs, lib, ... }:

with lib; with pkgs;

let # escape backslash with backslash!
    cfg = config.nx.wm;
in

{
  options.nx.wm = {
    modifier = mkOption {
      type = types.str;
      default = "Mod4";
      description = ''
        The modifier key used by the wm (i3)
      '';
    };
    # configFile = mkOption {
    #   type = types.path;
    #   default = pkgs.writeText "i3config" "#empty";
    #   description = ''
    #     This is the i3 config file (might be written with pkgs.writeText)
    #   '';
    # };
  };
}
