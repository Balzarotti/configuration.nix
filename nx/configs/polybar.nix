{writeText}:

let
colors.foreground = "#93a1a1";
colors.background = "#002b36";
colors.focused = "#b58900";
colors.urgent = "#dc322f";
colors.inactive = "#586e75";
colors.green = "#859900";

# ~/.config/polybar/config
in writeText "config" ''
; TODO: create an "external" bar
; that can be enabled while switching screen
; file: bin/monitors/*
[bar/bar]
monitor = eDP1
; what if monitor not found? use fallback
; monitor-fallback = DP1
; allows non-connected monitor
monitor-strict = false

; fixed-center = false
; bottom = true

; bar position
width = 100%
height = 19
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

; FIXME: decide
underline-size = 2
underline-color = ${colors.foreground}

padding-left = 0
padding-right = 0

module-margin-right = 0
module-margin-left = 0

font-0 = Fira Sans:size=8;0
font-1 = FontAwesome:size=10;0
font-2 = TakaoGothic:weight=bold:size=10;0
font-3 = FontAwesome:size=8;0

; FIXME
modules-left = i3 wlan eth xwindow
modules-center = date
modules-right = volume filesystem cpu presentation battery mail

; TODO: use backglight on another bar

; Don't seems to have effect?
; dim-value = 0.3

tray-position = right
tray-detached = false
; tray-maxsize =
tray-transparent = false
tray-background = ${colors.background}
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 0
tray-scale = 1.0

; Works even without
; wm-restack = i3
; override-redirect = false

; FIXME: don't know
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left = california &
click-middle =
click-right =
; change i3 workspace anywhere
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

double-click-left =
double-click-middle =
double-click-right =

; FIXME: don't seems to change
; [global/wm]
; margin-top = 0
; margin-bottom = 0

; FIXME: using default right now
[settings]
; The throttle settings lets the eventloop swallow up til X events
; if they happen within Y millisecond after first event was received.
; This is done to prevent flood of update event.
;
; For example if 5 modules emit an update event at the same time, we really
; just care about the last one. But if we wait too long for events to swallow
; the bar would appear sluggish so we continue if timeout
; expires or limit is reached.
throttle-output = 5
throttle-output-for = 10

; Time in milliseconds that the input handler will wait between processing events
throttle-input-for = 30

; Reload upon receiving XCB_RANDR_SCREEN_CHANGE_NOTIFY events
screenchange-reload = false

; Compositing operators
; @see: https://www.cairographics.org/manual/cairo-cairo-t.html#cairo-operator-t
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground =
format-background =
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

[module/i3]
type = internal/i3

; Show workspaces monitor specific only
pin-workspaces = true

; Keep them sorted without needing to see the useless number
; (strips the name ad ":"
strip-wsnumbers = true

; FIXME: Don't understand
;index-sort = true

; Disable scroll
; (enabled anywhere using the global fallback)
enable-scroll = false

; FIXME: might get useful some day
; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = true

format = <label-mode> <label-state>
label-mode-background = #e60053

label-visible-padding = 1
label-focused-padding = 1
label-unfocused-padding = 1
label-urgent-padding = 1

label-visible-underline = ${colors.foreground}
label-focused-underline = ${colors.focused}
label-urgent-underline = ${colors.urgent}
label-unfocused-underline =

[module/xwindow]
type = internal/xwindow
label = %title%
format-padding = 1
label-maxlen = 50
; format-foreground = #000
; format-background = #f00

[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0

format-connected-padding = 1
format-disconnected-padding = 1

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.foreground}
format-disconnected-underline = ${colors.inactive}

; FIXME
ramp-signal-0 = -
ramp-signal-1 = -
ramp-signal-2 = -
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-5 = 

; TODO: add click!

; FIXME: might be useful:  %local_ip%, %downspeed%, %upspeed%
label-connected = %essid%
label-disconnected = 
label-disconnected-foreground = ${colors.inactive}

[module/eth]
type = internal/network
interface = enp1s0
interval = 3.0

format-connected-padding = 1
format-disconnected-padding = 1

format-connected-underline = ${colors.foreground}
format-disconnected-underline = ${colors.inactive}

label-connected = 
label-disconnected = 
; FIXME: inactive is too dark
label-disconnected-foreground = ${colors.inactive}

[module/mpd]
type = internal/mpd

; connection details
host = 127.0.0.1
; port = 6600
password = "remoteMPD1299"

; time to sleep, default = 1
interval = 3

format-online = <label-song> <toggle>
format-offline = <label-offline>

label-offline =
label-offline-foreground = ${colors.inactive}

label-song = %artist% - %title%
label-song-maxlen = 50
label-song-ellipsis = true

; inverted since it wants them to be buttons
icon-play = "paused "
icon-pause = "playing "

; toggle-on-foreground =
; toggle-off-foreground =

[module/filesystem]
type = internal/fs
; default = 30
interval = 30

mount-0 = /
; mount-1 = /home
; mount-2 = /invalid/mountpoint

; Bug: wrong size (GB instead of MB)
label-mounted = %mountpoint% %free%
label-mounted-underline =
label-mounted-padding = 1

label-unmounted =
label-unmounted-foreground = ${colors.inactive}

[module/backlight]
type = internal/xbacklight

format = <label> <bar>
label = %percentage%%

bar-width = 10
bar-indicator = │
bar-indicator-foreground = #ff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = #55

[module/battery]
type = internal/battery
; ls -1 /sys/class/power_supply/
battery = BAT1
adapter = ACAD
; if inotify does not report anything, asks. default 5
poll-interval = 5
full-at = 98

; Don't like it to move <animation-charging>
format-charging = <label-charging>
format-charging-underline = ${colors.green}
format-charging-padding = 1
label-charging = %percentage%% (%time%)

format-discharging = <label-discharging>
format-discharging-underline =
format-discharging-padding = 1
label-discharging = %percentage%% (%time%)

format-full = 
format-full-underline = ${colors.green}
format-full-overline = ${colors.green}
format-full-padding = 1

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground}
; default: 750
animation-charging-framerate = 2000

; TODO: still not customized
[module/volume]
type = internal/volume

; <bar-volume>
format-volume = <ramp-volume> <label-volume>
label-volume = %percentage%%
ramp-volume-0 = " "
ramp-volume-1 =
ramp-volume-2 = " "

label-volume-foreground = ${colors.foreground}

label-muted = "  (%percentage%%)"
label-muted-foreground = ${colors.inactive}

bar-volume-width = 6
bar-volume-gradient = false
bar-volume-indicator = 
bar-volume-indicator-font = 4
bar-volume-indicator-foreground = ${colors.foreground}
bar-volume-fill = 
bar-volume-fill-font = 4
bar-volume-empty = 
bar-volume-empty-font = 4
bar-volume-empty-foreground = ${colors.inactive}

[module/cpu]
type = internal/cpu
format = <label>

; label = %{F${colors.foreground}  %percentage% %{F-}}
interval = 5
format-padding = 1

[module/date]
type = internal/date
date-alt =   %A %H:%M
date =   %y/%m/%d %H:%M:%S
interval = 5
format-underline =
;format-background = ${colors.background}
format-foreground = ${colors.foreground}
format-padding = 1
; still not supported
;click-right = california

; TODO: add music player ctrl etc
[bar/extra]
monitor = eDP1
monitor-strict = false

; fixed-center = false
bottom = true

; bar position
width = 30%
height = 30
offset-x = 35%
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

; FIXME: decide
underline-size = 2
underline-color = ${colors.foreground}

padding-left = 0
padding-right = 0

module-margin-right = 0
module-margin-left = 0

font-0 = pango:Fira Sans:size=8;0
font-1 = FontAwesome:size=10;0
font-2 = TakaoGothic:weight=bold:size=10;0
font-3 = FontAwesome:size=8;0

; FIXME
modules-left =
modules-center = mpd
modules-right = backlight

; TODO: use backglight on another bar

; Don't seems to have effect?
; dim-value = 0.3

; Works even without
wm-restack = i3
override-redirect = true

; FIXME: don't know
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
; change i3 workspace anywhere
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

double-click-left =
double-click-middle =
double-click-right =

; FIXME: don't seems to change
; [global/wm]
; margin-top = 0
; margin-bottom = 0

[bar/external]
; This bar is for the external monitor
monitor = DP1
; what if monitor not found? use fallback
; check!
monitor-fallback = HDMI
; allows non-connected monitor
monitor-strict = false

; fixed-center = false
; bottom = true

; bar position
width = 100%
height = 20
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

; FIXME: decide
underline-size = 2
underline-color = ${colors.foreground}

padding-left = 0
padding-right = 0

module-margin-right = 0
module-margin-left = 0

font-0 = pango:Fira Sans:size=8;0
font-1 = FontAwesome:size=10;0
font-2 = TakaoGothic:weight=bold:size=10;0
font-3 = FontAwesome:size=8;0

; FIXME
modules-left = i3
modules-center = date
modules-right =

; Required for i3
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
; change i3 workspace anywhere
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

double-click-left =
double-click-middle =
double-click-right =

; FIXME: don't seems to change
; [global/wm]
; margin-top = 0
; margin-bottom = 0

[module/mail]
type = custom/script
interval = 5
format = <label>
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.green}

click-left = notify-send -t 1000 "Updating emails" && update-mails &
exec = MESS=" "; COUNT=$(notmuch count tag:unread); if [ "$COUNT" != "0" ]; then MESS="$MESS$COUNT"; fi; echo $MESS

[module/presentation]
; updated with polybar-msg hook presentation 1
type = custom/ipc
initial = 1
underline-color = ${colors.foreground}
hook-0 = bin/active-modes ".presentation-mode" ""
click-left = bin/toggle-presentation
''
