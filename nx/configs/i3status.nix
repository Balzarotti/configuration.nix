{writeText}:

writeText "i3status" ''

general {
        output_format = "i3bar"
        colors = true
        interval = 5
}

order += "path_exists touchscreen"
order += "path_exists touchpad"
order += "path_exists mute-mode"
order += "path_exists presentation"
order += "volume master"
order += "tztime local"
order += "disk /"
order += "battery 0"
order += "battery 1"
order += "load"

path_exists touchpad {
	path = "/tmp/.touchpad-disabled"
	format = "touchpad disabled"
 #Big screen
#	need version 2.10:
	format_down = "" #laptop
}

path_exists touchscreen {
	path = "/tmp/.touchscreen-disabled"
	format = "touchscreen disabled"
 #Big screen
#	need version 2.10:
	format_down = "" #laptop
}

path_exists presentation {
	path = "/tmp/.presentation-mode"
	format = "" #Big screen
#	need version 2.10:
#	format_down = "" #laptop
	format_down = "" #laptop
}

path_exists mute-mode {
	path = "/tmp/.mute-notification-mode"
	format = ""
#	need version 2.10:
#	format_down = ""
	format_down = ""
}

battery 0 {
        format = "%status %percentage %remaining %emptytime"
	hide_seconds = true
        format_down = ""
        status_chr = ""
        status_bat = ""
        status_full = ""
        path = "/sys/class/power_supply/BAT%d/uevent"
#	last_full_capacity = true #Use this for relative charge
	integer_battery_capacity = false
	threshold_type = percentage
        low_threshold = 20
}
battery 1 {
        format = "%status %percentage %remaining %emptytime"
	hide_seconds = true
        format_down = ""
        status_chr = ""
        status_bat = ""
        status_full = ""
        path = "/sys/class/power_supply/BAT%d/uevent"
#	last_full_capacity = true #Use this for relative charge
	integer_battery_capacity = false
	threshold_type = percentage
        low_threshold = 20
}

tztime local {
        format = " %Y-%m-%d   %H:%M:%S"
}

load {
        format = "%1min"
}

disk "/" {
        format = " %free"
}

#2.10: upgrade this:
volume master {
        format = " %volume"
        format_muted = " (%volume)"
	device = "pulse"
#        device = "default"
        mixer = "Master"
        mixer_idx = 0
}

''
