{ config, pkgs, ... }:

{ imports =
    [
    # ./bin
      ./configs
      ./emacs
      ./mail
      # ./patches
      ./rtconf
      ./style
      # ./synergy # FIXME
      ./wm
    ];
}
