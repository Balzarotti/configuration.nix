{ config, pkgs, ... }:

let

  cfg = config.my-emacs;

#  hexrgb = pkgs.stdenv.lib.overrideDerivation pkgs.emacs24PackagesNg.hexrgb (oldAttrs : {
#         sha256 = "xlxzd1rqq8wlkhd5q2wdbn83zahw81r4qzdz2wdcq";
#       });

in  {

  # Other dependencies
  environment.systemPackages = with pkgs.emacsPackagesNg; [
    emacs
    # package setup

    melpaPackages.use-package
    melpaPackages.load-relative
    # nix
    melpaPackages.nix-sandbox
    melpaPackages.company-nixos-options
    # melpaPackages.nix-mode

    # git

    melpaPackages.magit pkgs.gnupg1 melpaPackages.with-editor
    melpaPackages.git-gutter
    # history
    melpaPackages.undo-tree
    melpaPackages.git-timemachine

    # ivy
    # (those broken in melpaPackages, using Stable)
    melpaStablePackages.swiper # counsel
    melpaStablePackages.ivy
    melpaStablePackages.wgrep  # used by ivy-occur

    melpaStablePackages.ivy-hydra

    # company
    melpaPackages.company

    # behaviour
    melpaPackages.dashboard
    melpaPackages.hungry-delete
    melpaPackages.multiple-cursors
    melpaPackages.aggressive-indent
    melpaPackages.avy
    melpaPackages.fiplr
    melpaPackages.which-key
    melpaPackages.switch-window
    melpaPackages.ggtags             pkgs.global pkgs.universal-ctags
    melpaPackages.dumb-jump
    melpaPackages.projectile
    melpaPackages.counsel-projectile
    melpaPackages.yasnippet
    melpaPackages.auto-yasnippet
    melpaPackages.smart-tabs-mode
    melpaPackages.vimish-fold

    # helpers
    melpaPackages.smart-comment
    melpaPackages.crux
    melpaPackages.scratch
    melpaPackages.bool-flip
    melpaPackages.expand-region

    # theme
    # melpaPackages.color-theme-solarized
    melpaPackages.solarized-theme
    melpaPackages.leuven-theme # white org mode
    melpaPackages.nyan-mode              pkgs.mplayer

    # apparence
    melpaPackages.smooth-scrolling
    melpaPackages.perspective
    melpaPackages.powerline
    melpaPackages.highlight-symbol
    melpaPackages.fic-mode
    melpaPackages.column-enforce-mode
    melpaPackages.linum-relative
    melpaPackages.rainbow-identifiers
    elpaPackages.rainbow-mode # Hex color codes

    # writing
    melpaPackages.synosaurus
    melpaPackages.wordnut                pkgs.wordnet
    melpaPackages.langtool
    melpaPackages.writegood-mode

    melpaPackages.writeroom-mode
    melpaPackages.wc-mode
    melpaPackages.cloc # count lines of  pkgs.cloc
    melpaPackages.lorem-ipsum

    # papers
    melpaPackages.pdf-tools # seems that binary are no longer installed?
    # melpaStablePackages.pdf-tools # this one elisp only
    melpaPackages.interleave
    melpaPackages.gscholar-bibtex
    melpaPackages.ivy-bibtex
    melpaPackages.org-ref
    melpaPackages.org-pdfview
    melpaPackages.magic-latex-buffer
    melpaPackages.slirm
    melpaPackages.org-mind-map

    # org
    pkgs.gcc6 # g++ required for compiling c/c++ source code in org files
    pkgs.imagemagick # required to resize images
    melpaPackages.org-bullets
    melpaPackages.org-download
    # org-plot/gnuplot
    melpaPackages.gnuplot-mode pkgs.gnuplot
    melpaPackages.gnuplot
    # ox-org
    melpaPackages.ox-reveal melpaPackages.htmlize
    melpaPackages.org-page
    # melpaPackages.worf

    # programming
    melpaPackages.ess
    melpaPackages.meson-mode
    melpaStablePackages.lua-mode
    melpaPackages.arduino-mode
    melpaPackages.faust-mode
    melpaPackages.dot-mode         pkgs.graphviz
    # rust
    melpaPackages.rust-mode
    melpaPackages.cargo pkgs.rustfmt
    # not available but suggested: # pkgs.clippy # pkgs.cargo-check

    # programming-web
    melpaPackages.emmet-mode
    melpaPackages.web-mode
    melpaPackages.jsx-mode
    melpaPackages.jss

    # internet
    melpaPackages.ember-mode
    melpaPackages.yarn-mode
    melpaPackages.skewer-mode
    melpaPackages.impatient-mode
    melpaPackages.react-snippets
    melpaPackages.php-mode
    # melpaPackages.palette # hex2rgb broken hash

    # languages
    # melpaPackages.csv-mode
    melpaPackages.json-mode
    melpaPackages.yaml-mode
    melpaPackages.markdown-mode
    # programming-java
    melpaPackages.gradle-mode
    melpaPackages.eclim melpaPackages.company-emacs-eclim

    # linters
    melpaPackages.flycheck-julia
    # debugging
    melpaPackages.realgud

    # programming c++ (and c)
    # stable since it must match the irony-server version
    melpaStablePackages.irony    pkgs.clang pkgs.irony-server
    melpaPackages.company-irony melpaPackages.flycheck-irony
    melpaPackages.flymake-google-cpplint
    # melpaPackages.rtags
    # FIXME: check one by one
    # melpaPackages.cmake-ide melpaPackages.cpputils-cmake
    pkgs.rtags

    # organizer
    melpaPackages.notmuch
    org-plus-contrib                         # required for org-notmuch
    melpaPackages.elfeed
    melpaPackages.elfeed-org
    melpaPackages.twittering-mode pkgs.gnupg # used for encryption
    melpaPackages.dired-details
    melpaPackages.bbdb

    # extra
    melpaPackages.kodi-remote
    melpaPackages.emms
    melpaPackages.elf-mode
    melpaPackages.iasm-mode
    melpaPackages.pcap-mode
    melpaPackages.web
    melpaPackages.xkcd
    melpaPackages.typing
    # melpaPackages.vimgolf
    melpaPackages.spray

#     iy-go-to-char

#     typo #typografical editing, typo-mode

    melpaPackages.org-caldav

#     # c/c++
#     # Not in melpa
#     # doxymacs

#     #
#     visual-regexp
#     visual-regexp-steroids
#     imenu-anywhere
#     airline-themes
#     # git
##     super-save
#     sr-speedbar #speed bar in current frame

#     openwith
#     zotxt
#     ox-pandoc

#     # nix <3
#     # nix-emacs repo:
#     nixos-options
#     helm-nixos-options

#     company-math
#     key-chord
#     gist

#     # keyfreq # no longer needed
#     ox-impress-js
#     # ox-cv # moderncv export
#     # https://github.com/mylese/ox-cv/blob/master/ox-cv.el
#     android-mode

#     iedit
#     anything
#     ecb
#     popup
#     typescript-mode
#     ledger-mode

#     # languages
#     toml-mode
#     spice-mode # circuits
#     ob-spice
#     # plantuml
#     pkgs.plantuml
#     lyrics

#     # Extra
    pkgs.aspell pkgs.aspellDicts.en pkgs.aspellDicts.it
    (pkgs.texlive.combine  { inherit (pkgs.texlive) scheme-medium
    # org-mode
    environ
    wrapfig
    capt-of

    acmart totpages
    latexmk
    dvipng;})
    pkgs.xpdf # dependency of org-ref
  ];
  fonts.fonts = with pkgs; [ hack-font ];

  programs.zsh.interactiveShellInit = ''
  mkdir -p $HOME/.emacs.d/
  # echo '(load-file "$ import ../configs/emacs.nix")' > $HOME/.emacs.d/init.el
  '';
}
