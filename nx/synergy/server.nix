{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ synergy quicksynergy ];
  networking.firewall.allowedTCPPorts = [ 24800 ];
  services.synergy.server = {
    enable = false;
    screenName = "lenovoE31";
    address = "0.0.0.0";
    autoStart = true;
  };
}
